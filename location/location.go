package location

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"strings"
)

type Location struct {
	PageUrl url.URL
	LocationData
}

type LocationData struct {
	CorporateCode string
	Id            int
}

func NewLocation(sourceUrl url.URL) *Location {
	l := new(Location)
	l.PageUrl = sourceUrl

	return l
}

func (loc *Location) PopulateLocationFromRemote() (jsonData map[string]interface{}, err error) {
	jsonUrl, err := loc.JsonUrlString()

	if err != nil {
		return nil, err
	}

	res, err := http.Get(jsonUrl)

	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &jsonData)

	if err != nil {
		return nil, err
	}

	var d LocationData
	err = json.Unmarshal(body, &d)

	if err != nil {
		return nil, err
	}

	loc.CorporateCode = d.CorporateCode
	loc.Id = d.Id

	return jsonData, err
}

func (thisLocation *Location) equals(thatLocation *Location) bool {
	if thisLocation.PageUrl.String() != thatLocation.PageUrl.String() {
		return false
	}

	if thisLocation.CorporateCode != thatLocation.CorporateCode {
		return false
	}

	if thisLocation.Id != thatLocation.Id {
		return false
	}

	return true
}

func (loc *Location) JsonUrlString() (string, error) {
	if loc.PageUrl.Scheme == "" || loc.PageUrl.Host == "" || loc.PageUrl.Path == "" {
		return "", errors.New("Bad Location URL")
	} else {
		htmlUrl, err := url.Parse(loc.PageUrl.String())

		if err != nil {
			return "", err
		}

		htmlUrl.Path = strings.TrimSuffix(htmlUrl.Path, path.Ext(htmlUrl.Path)) + ".json"

		return htmlUrl.String(), nil
	}
}

func AppendUniquely(locs []Location, loc *Location) []Location {
	for _, l := range locs {
		if l.equals(loc) {
			return locs
		}
	}
	return append(locs, *loc)
}
