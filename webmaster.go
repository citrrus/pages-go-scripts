package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"github.com/cheggaaa/pb"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/webmasters/v3"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

const (
	errorType     = "notFound"
	errorPlatform = "web"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var (
	siteDomain = flag.String("site", "", "**REQUIRED** The Pages Site that you want to Check.")
	Usage      = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}
)

func createSiteUrl() (siteUrl *url.URL) {
	flag.Parse()

	if *siteDomain == "" {
		Usage()
		return
	}

	siteUrl, err := url.Parse(*siteDomain)

	check(err)

	if siteUrl.Scheme == "" || siteUrl.Host == "" {
		panic("Please provide a fully qualified URL, e.g. http://www.ftdflorists.com")
	}

	return siteUrl
}

func createService() (webmastersService *webmasters.Service) {
	authData, err := ioutil.ReadFile("auth_config.json")

	check(err)

	conf, err := google.JWTConfigFromJSON(authData, "https://www.googleapis.com/auth/webmasters")

	check(err)

	client := conf.Client(oauth2.NoContext)

	service, err := webmasters.New(client)

	check(err)

	return service
}

func getCrawlErrorSamples(siteUrl url.URL, service *webmasters.Service) (crawlErrors []*webmasters.UrlCrawlErrorsSample) {
	listRequest := service.Urlcrawlerrorssamples.List(siteUrl.String(), errorType, errorPlatform)

	listResp, err := listRequest.Do()

	check(err)

	return listResp.UrlCrawlErrorSample
}

func checkUrl(urlString string) (statusCode int) {
	res, err := http.Get(urlString)

	defer res.Body.Close()

	check(err)

	return res.StatusCode
}

func checkCrawlErrors(testUrl url.URL, crawlErrors []*webmasters.UrlCrawlErrorsSample) (fixedCrawlErrors []*webmasters.UrlCrawlErrorsSample, stillBrokenUrls []*webmasters.UrlCrawlErrorsSample) {
	requestProgress := pb.StartNew(len(crawlErrors))
	requestProgress.ShowTimeLeft = true
	requestProgress.SetMaxWidth(120)

	fmt.Printf("Re-testing %v sites", len(crawlErrors))

	for _, sampleCrawlError := range crawlErrors {
		testUrl.Path = sampleCrawlError.PageUrl

		code := checkUrl(testUrl.String())

		switch code {
		case 200, 301, 302:
			fixedCrawlErrors = append(fixedCrawlErrors, sampleCrawlError)
		default:
			stillBrokenUrls = append(stillBrokenUrls, sampleCrawlError)
		}

		requestProgress.Increment()
	}

	requestProgress.FinishPrint("Finished Testing Sites")

	return fixedCrawlErrors, stillBrokenUrls
}

func outputUrlsToFix(rootUrl url.URL, brokenUrls []*webmasters.UrlCrawlErrorsSample) {
	if len(brokenUrls) > 0 {
		var fileName = "broken_urls_" + rootUrl.Host + "_" + time.Now().Local().Format("20060102150405") + ".csv"

		outputFile, err := os.Create(fileName)

		check(err)

		defer outputFile.Close()

		writer := csv.NewWriter(outputFile)

		defer writer.Flush()

		fmt.Printf("Writing results to %v\n", fileName)

		for _, brokenUrl := range brokenUrls {
			var row = make([]string, 1)

			row[0] = brokenUrl.PageUrl

			pathComponents := strings.Split(brokenUrl.PageUrl, "/")

			for _, pathComponent := range pathComponents {
				row = append(row, pathComponent)
			}

			writer.Write(row)
		}
	} else {
		fmt.Println("No broken urls to output")
	}
}

func markGoodUrlsAsFixed(siteUrl url.URL, fixed []*webmasters.UrlCrawlErrorsSample, service *webmasters.Service) {
	if len(fixed) > 0 {
		progress := pb.StartNew(len(fixed))
		progress.ShowTimeLeft = true
		progress.SetMaxWidth(120)

		for _, fixedUrl := range fixed {

			markAsFixed := service.Urlcrawlerrorssamples.MarkAsFixed(siteUrl.String(), fixedUrl.PageUrl, errorType, errorPlatform)

			err := markAsFixed.Do()

			check(err)

			progress.Increment()
		}

		progress.FinishPrint("Finished Marking Urls As Fixed")
	} else {
		fmt.Println("No fixed urls to update")
	}
}

func main() {
	siteUrl := createSiteUrl()
	service := createService()

	fmt.Printf("Checking %v for crawlErrors\n", siteUrl)

	crawlErrors := getCrawlErrorSamples(*siteUrl, service)

	fmt.Printf("Found %v Crawl Errors for %v\n", len(crawlErrors), siteUrl)

	fixed, broken := checkCrawlErrors(*siteUrl, crawlErrors)

	fmt.Printf("%v Errors are now Fixed, %v remain Broken\n", len(fixed), len(broken))

	markGoodUrlsAsFixed(*siteUrl, fixed, service)

	outputUrlsToFix(*siteUrl, broken)
}
