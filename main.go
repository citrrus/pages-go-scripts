package main

import (
	"./location"
	"encoding/csv"
	"flag"
	"fmt"
	"github.com/cheggaaa/pb"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

var (
	siteDomain      = flag.String("site", "", "**REQUIRED** The Pages Site that you want to Check.")
	outputGoodLinks = flag.Bool("showGoodLinks", false, "The script will output links that return 200 status codes")
	debugMode       = flag.Bool("debug", false, "The script will only test 200 pages")
	threads         = flag.Int("threadCount", 10, "How many simultaneous workers to use")
	outputFileName  = flag.String("outputFileName", "output.csv", "The name of the result file")
)

var Usage = func() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func createSiteUrl() (siteUrl *url.URL) {
	siteUrl, err := url.Parse(*siteDomain)

	check(err)

	if siteUrl.Scheme == "" {
		panic("Please provide a fully qualified Url, e.g. http://google.com")
	}

	return siteUrl
}

func getLocationUrls(siteUrl url.URL) (urls []url.URL) {
	siteUrl.Path = "locationPages.csv"

	resp, err := http.Get(siteUrl.String())

	check(err)

	defer resp.Body.Close()

	csvReader := csv.NewReader(resp.Body)

	csvData, err := csvReader.ReadAll()

	check(err)

	for lineNum, row := range csvData {

		if lineNum == 0 {
			continue //skip header
		}

		if len(row) == 0 {
			fmt.Println("Something is wrong with this CSV File")
			return nil
		}

		htmlUrl, err := url.Parse(row[len(row)-1])

		check(err)

		urls = append(urls, *htmlUrl)
	}

	return urls
}

func checkUrl(urlString string) (statusCode int, htmlError error) {
	res, err := http.Get(urlString)

	if err != nil {
		return -1, err
	}

	res.Body.Close()

	return res.StatusCode, nil
}

func findUrls(json interface{}) (foundUrlStrings []string) {
	fStrings := make([]string, 0)

	switch data := json.(type) {
	case map[string]interface{}:
		for _, value := range data {
			for _, foundUrl := range findUrls(value) {
				fStrings = append(fStrings, foundUrl)
			}
		}
	case []interface{}:
		for _, v := range data {
			for _, foundUrl := range findUrls(v) {
				fStrings = append(fStrings, foundUrl)
			}
		}
	case string:
		stringUrl, err := url.Parse(data)

		if err == nil && stringUrl.Scheme != "" {
			fStrings = append(fStrings, stringUrl.String())
		}
	}

	return fStrings
}

func buildUrlMap(pageUrls []url.URL) (urlMap map[string][]location.Location) {
	fmt.Println("Building URL Map")
	urlMap = make(map[string][]location.Location)

	var limit int

	if *debugMode == true {
		limit = 200
	} else {
		limit = len(pageUrls)
	}

	prog := pb.StartNew(limit)
	for i, page := range pageUrls {

		if *debugMode == true && i >= limit {
			break
		}

		l := location.NewLocation(page)
		jsonMap, err := l.PopulateLocationFromRemote()

		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		links := findUrls(jsonMap)

		for _, contentUrl := range links {
			if sourceLocations, present := urlMap[contentUrl]; present {
				urlMap[contentUrl] = location.AppendUniquely(sourceLocations, l)
			} else {
				urlMap[contentUrl] = []location.Location{*l}
			}
		}
		prog.Increment()
	}
	prog.FinishPrint("Finished Building Map")

	return urlMap
}

type urlTestRequest struct {
	testUrl         string
	sourceLocations []location.Location
}

type urlTestResult struct {
	urlTestRequest
	responseCode int
	errorString  string
}

func worker(requestChannel chan urlTestRequest, resultChannel chan urlTestResult) {
	for testRequest := range requestChannel {

		code, err := checkUrl(testRequest.testUrl)

		var errorString = ""

		if err != nil {
			errorString = err.Error()
		}

		resultChannel <- urlTestResult{testRequest, code, errorString}
	}

	return
}

func asyncCheckUrls(urlConfigMap map[string][]location.Location) (results []urlTestResult) {
	wc := make(chan urlTestRequest)
	rc := make(chan urlTestResult)

	fmt.Println("Checking URLS")

	var badResultCount = 0

	for i := 0; i < *threads; i++ {
		go worker(wc, rc)
	}

	go func() {
		for urlToCheck, sourceLocations := range urlConfigMap {
			wc <- urlTestRequest{urlToCheck, sourceLocations}
		}
		close(wc)
	}()

	prog := pb.StartNew(len(urlConfigMap))
	prog.ShowTimeLeft = true
	prog.SetMaxWidth(120)

	for result := range rc {
		prog.Increment()

		if result.responseCode != 200 {
			badResultCount++
		}

		results = append(results, result)

		if len(results) == len(urlConfigMap) {
			break
		}
	}

	close(rc)

	prog.FinishPrint(fmt.Sprintf("Found %v Bad Urls out of %v tested\n", badResultCount, len(urlConfigMap)))

	return results
}

func main() {
	flag.Parse()

	if *siteDomain == "" {
		Usage()
		return
	}

	rootUrl := createSiteUrl()

	outputFile, err := os.Create(*outputFileName + ".csv")

	check(err)

	defer outputFile.Close()

	writer := csv.NewWriter(outputFile)

	defer writer.Flush()

	urls := getLocationUrls(*rootUrl)
	uniqueUrlMap := buildUrlMap(urls)

	writer.Write([]string{
		"Client Store ID",
		"Yext Location ID",
		"Location Page URL",
		"Tested URL",
		"HTTP Response Code",
		"Error Message",
	})

	for _, result := range asyncCheckUrls(uniqueUrlMap) {

		for _, sl := range result.sourceLocations {
			if result.responseCode == 200 && *outputGoodLinks == false {
				continue
			}
			var row = []string{
				sl.CorporateCode,
				strconv.Itoa(sl.Id),
				sl.PageUrl.String(),
				result.testUrl,
				strconv.Itoa(result.responseCode),
				result.errorString,
			}

			writer.Write(row)
		}

		writer.Flush()
	}
}
